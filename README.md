# Spinal HDL Utilities

This is a collection of some SpinalHDL Utilities I made for myself.

## How to use

Simply copy the hw/spinal/utils directory in your SpinalHDL Project and use it.

Documentation for each Component is included in its source file.

## License

This Software is distributed under the MPL2 License.

For an overview on what this license means, look at the [FAQ](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)

