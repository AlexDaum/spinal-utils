/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package myutils

import spinal.core._

/**
 * Strobe Generator
 *
 * Generates a Strobe with specified frequency (best integer divisor from clock)
 *
 * @param clock The clock frequency, used to calculate the division factor
 * @param frequency The desired frequency of the Strobe
 *
 * 
 *
 * | Port   | Direction | Type  | Description              |
 * | ---    | ---       | ---   | ---                      |
 * | strobe | out       | Bool: | High for one clock cycle |
 *
 *
 * @author Alexander Daum
 */
case class StrobeGen(clock: HertzNumber, frequency: HertzNumber) extends Component {
  val io = new Bundle() {
    val strobe = out Bool()
  }

  val clockScaler = ((clock / frequency) + 0.5).toBigInt()
  val clockScalerWidth = log2Up(clockScaler) bits

  val counter = Reg(UInt(clockScalerWidth)) init(0)
  val strobe = Reg(Bool()) init(False)

  when(counter === clockScaler) {
    counter := 0
    strobe := True
  } otherwise {
    counter := counter + 1
    strobe := False
  }

  io.strobe := strobe

}
