/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package myutils

import spinal.core._

/**
 * Component to decode a hexadecimal value to a 7-Segment Display.
 * The Segment order is illustrated below.
 *
 *  aaa
 * f   b
 * f   b
 *  ggg
 * e   c
 * e   c
 *  ddd
 *
 * The display output is organized in descending order, so that a is the LSB
 *
 * Bitorder: gfedcba
 *
 * Ports:
 *
 * | Port    | Direction | Type    | Description    |
 * | ---     | ---       | ---     | ---            |
 * | number  | in        | UInt(4) | Input Number   |
 * | display | out       | Bits(7) | Segment output |
 *
 * @author Alexander Daum
 */
case class HexDigit() extends Component {
  val io = new Bundle() {
    val number = in UInt(4 bits)
    val display = out Bits(7 bits)
  }

  io.display := io.number.mux(
    0  -> B"0111111",
    1  -> B"0000110",
    2  -> B"1011011",
    3  -> B"1001111",
    4  -> B"1100110",
    5  -> B"1101101",
    6  -> B"1111101",
    7  -> B"0000111",
    8  -> B"1111111",
    9  -> B"1101111",
    10 -> B"1110111",
    11 -> B"1111100",
    12 -> B"1011000",
    13 -> B"1011110",
    14 -> B"1111001",
    15 -> B"1110001",
  )
}
