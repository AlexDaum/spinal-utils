/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package myutils

import spinal.core._

/**
 * Component for building decimal Counters.
 *
 * Counts from 0 to 9 and generates a Strobe output for easy chaining.
 * The outstrobe will be set in the same Cycle, that the Counter returned to 0.
 *
 * The Counter only counts up on clockcycles when the strobe input is True.
 * A multi-digit decimal Counter can be created by chaining multiple BCDCounter instances and connecting
 * each strobe input to the previous outstrobe.
 *
 * Ports:
 *
 * | Port      | Direction | Type    | Description                                                  |
 * | ---       | ---       | ---     | ---                                                          |
 * | strobe    | in        | Bool    | Counter only counts up when this is true                     |
 * | count     | out       | UInt(4) | The current count, always between 0 and 9                    |
 * | outstrobe | out       | Bool    | True for one Cycle when the Counter wraps. Used for chaining |
 *
 * @author Alexander Daum
 */
case class BCDCounter() extends Component{
  val io = new Bundle() {
    val strobe = in Bool()
    val count = out UInt(4 bits)
    val outstrobe = out Bool()
  }

  val cntReg = Reg(UInt(4 bits)) init(0)
  val strobe = Reg(Bool()) init(False)

  strobe := False

  when(io.strobe) {
    when (cntReg === 9) {
      cntReg := 0
      strobe := True
    } otherwise {
      cntReg := cntReg + 1
    }
  }

  io.count := cntReg
  io.outstrobe := strobe
}
