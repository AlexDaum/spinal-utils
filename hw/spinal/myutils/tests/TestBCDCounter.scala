/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package myutils.tests

import spinal.core.sim._
import myutils._
import scala.language.reflectiveCalls

object TestBCDCounter extends App {
  SimConfig.withGhdl.withWave.compile(new BCDCounter()).doSim{dut =>
    dut.clockDomain.forkStimulus(period = 10)
    for (cnt <- 0 to 9) {
      dut.io.strobe #= true
      dut.clockDomain.waitSampling()
      assert(dut.io.count.toInt == cnt)
      assert(!dut.io.outstrobe.toBoolean)
    }
    dut.clockDomain.waitSampling()
    assert(dut.io.outstrobe.toBoolean)
    for (cnt <- 1 to 9) {
      dut.io.strobe #= true
      dut.clockDomain.waitSampling()
      assert(dut.io.count.toInt == cnt)
      assert(!dut.io.outstrobe.toBoolean)
    }
    dut.clockDomain.waitSampling()
    for (cnt <- 0 to 100) {
      dut.io.strobe #= false
      dut.clockDomain.waitSampling()
      assert(dut.io.count.toInt == 1,
        "Expected 1, Got %d" format(dut.io.count.toInt))
      assert(!dut.io.outstrobe.toBoolean)
    }
  }
}
